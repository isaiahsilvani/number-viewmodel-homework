package com.example.numberhomev2.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.numberhomev2.model.repository.NumberRepo
import com.example.numberhomev2.views.fragments.OptionsFragment.OptionsState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

val numberRange = (1..200).toList()

class NumberViewModel: ViewModel() {
    val TAG = "NumberView Model"

    // Create variable to manipulate the number repo
    private val repo: NumberRepo = NumberRepo.getInstance(1)

    private var _optionsState: MutableLiveData<OptionsState> = MutableLiveData(OptionsState())
    val optionsState: LiveData<OptionsState> get() = _optionsState
    // Create variable to manipulate the live state data in Options fragment

    fun handleSelection(option: String) {
        val number = when (option) {
            "ODD" -> odd().random()
            "RANDOM" -> numberRange.random()
            "EVEN" -> even().random()
            else -> throw error("WHAT THE HECK HAPPENED")
        }
        viewModelScope.launch {
            with(_optionsState) {
                Log.e(TAG, "number input loading....")
                value = value?.copy(isLoading = true)
                delay(1000)
                value = value?.copy(isLoading = false)
                value = value?.copy(number = number)
            }
        }
    }


    fun even(): MutableList<Int> {
        val evenNumbers: MutableList<Int> = mutableListOf()
        for (number in numberRange) {
            if (number % 2 == 0) evenNumbers.add(number)
        }
        return evenNumbers
    }

    fun odd(): MutableList<Int> {
        val oddNumbers: MutableList<Int> = mutableListOf()
        for (number in numberRange) {
            if (number % 2 != 0) oddNumbers.add(number)
        }
        return oddNumbers
    }

}