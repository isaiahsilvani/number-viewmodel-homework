package com.example.numberhomev2.model.repository

import android.util.Log

class NumberRepo private constructor (
    someNumber: Int
        ) {
    val TAG = "NumberRepo"

    val previousNumbers = mutableListOf(
        someNumber
    )

    fun addNumber(number: Int) {
        Log.e(TAG, "addedData: adding $number to previous numbers.")
        previousNumbers.add(number)
    }

    companion object {
        private var instance: NumberRepo? = null

        fun getInstance(theNumber: Int): NumberRepo = NumberRepo(theNumber)
    }
}