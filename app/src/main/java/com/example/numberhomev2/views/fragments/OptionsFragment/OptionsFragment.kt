package com.example.numberhomev2.views.fragments.OptionsFragment

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.numberhomev2.R
import com.example.numberhomev2.databinding.OptionsFragmentBinding
import com.example.numberhomev2.viewmodel.NumberViewModel

val TAG = "Options Fragment"

class OptionsFragment: Fragment(R.layout.options_fragment) {

    lateinit var binding: OptionsFragmentBinding
    lateinit var optionsState: OptionsState
    // Source of the data
    private val viewModel by viewModels<NumberViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = OptionsFragmentBinding.inflate(inflater, container, false)
        initViews()
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        // Initialize the observer
        with(binding) {
            viewModel.optionsState.observe(viewLifecycleOwner) { theState ->
                if (theState.number != null) {
                    val number = theState.number!!.toInt()
                    println(number)
                    header.textSize = 80.0F
                    header.text = theState.number.toString()
                    if (number % 2 == 0) {
                        header.setTextColor(Color.BLUE)
                    } else {
                        header.setTextColor(Color.RED)
                    }
                }
                if (theState.isLoading) {
                    loadingText.text = "loading..."
                } else {
                    loadingText.text = ""
                }
            }
        }

    }

    private fun initViews() {
        with(binding) {

            val buttons = listOf(evenBtn, oddBtn, randomBtn)
            for (button in buttons) {
                button.setOnClickListener {
                    val option = button.text.toString()
                    viewModel.handleSelection(option)
                }
            }
        }
    }
}