package com.example.numberhomev2.views.fragments.OptionsFragment
// SET YOUR INITIAL STATE HERE BRUH
data class OptionsState(
    var isLoading: Boolean = false,
    var errorMsg:String = "ERROR OMG",
    var number: Int? = null
)
