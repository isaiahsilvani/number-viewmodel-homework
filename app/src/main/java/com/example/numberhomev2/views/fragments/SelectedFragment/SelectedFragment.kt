package com.example.numberhomev2.views.fragments.SelectedFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.numberhomev2.R
import com.example.numberhomev2.databinding.SelectedFragmentBinding

class SelectedFragment: Fragment(R.layout.selected_fragment) {

    private lateinit var binding: SelectedFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = SelectedFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }
}